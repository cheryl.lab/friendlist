## Description

Implement Friend List using RxSwift with MVVM pattern.

## Installation

Install the dependencies in the project.

```
$ pod install
```

Make sure to always open the **Xcode workspace** instead of the project file when building this project.

```
$ open FriendList.xcworkspace
```

## Libraries

- [Alamofire](https://github.com/Alamofire/Alamofire)
- [RxSwift](https://github.com/ReactiveX/RxSwift)
- [IQKeyboardManagerSwift](https://github.com/hackiftekhar/IQKeyboardManager)
