//
//  FriendListCell.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/2.
//

import UIKit

class FriendListCell: UITableViewCell {
    
    @IBOutlet private weak var starImageView: UIImageView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var moreView: UIView!
    @IBOutlet private weak var invitedView: UIView!
    @IBOutlet private weak var invitedLabel: UILabel!
    @IBOutlet private weak var dividingLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(with friend: Friend) {
        starImageView.isHidden = !friend.isTop
        nameLabel.text = friend.name
        moreView.isHidden = friend.status != .accepted
        invitedView.isHidden = friend.status != .invited
    }
}

private extension FriendListCell {
    
    func setupUI() {
        selectionStyle = .none
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        
        nameLabel.textColor = .textBlack
        
        invitedView.layer.borderWidth = 1
        invitedView.layer.borderColor = UIColor.borderLightGray.cgColor
        invitedView.layer.cornerRadius = 3
        
        invitedLabel.text = "邀請中"
        invitedLabel.textColor = .textGray
        
        dividingLineView.backgroundColor = .dividingLine
    }
}
