//
//  InvitationsCell.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/2.
//

import UIKit

class InvitationsCell: UITableViewCell {
    
    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(with friend: Friend) {
        nameLabel.text = friend.name
    }
}

private extension InvitationsCell {
    
    func setupUI() {
        selectionStyle = .none
        
        bgView.layer.cornerRadius = 8
        
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOffset = .init(width: 0, height: 4)
        bgView.layer.shadowRadius = 5
        bgView.layer.shadowOpacity = 0.08
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        
        nameLabel.textColor = .textBlack
        
        messageLabel.text = "邀請你成為好友：）"
        messageLabel.textColor = .textGray
    }
}
