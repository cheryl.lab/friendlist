//
//  FriendListViewController.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/1.
//

import UIKit
import RxSwift
import RxCocoa

class FriendListViewController: UIViewController {
    
    @IBOutlet private weak var manView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var idLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    
    @IBOutlet private weak var noFriendsView: UIView!
    @IBOutlet private weak var noFriendsTitleLabel: UILabel!
    @IBOutlet private weak var noFriendsMessageLabel: UILabel!
    
    @IBOutlet private weak var invitationsView: UIView!
    @IBOutlet private weak var invitationsTableView: UITableView!
    
    @IBOutlet private weak var searchBgView: UIView!
    @IBOutlet private weak var searchTextField: UITextField!
    
    @IBOutlet private weak var friendListStackView: UIStackView!
    @IBOutlet private weak var friendListTableView: UITableView!
    
    private let viewModel: FriendListViewModel?
    
    private let bag = DisposeBag()
    
    init(with viewModel: FriendListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: FriendListViewController.name, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindRx()
        viewModel?.start()
    }
}

private extension FriendListViewController {
    
    func setupUI() {
        nameLabel.textColor = .textBlack
        
        idLabel.textColor = .textBlack
        
        noFriendsTitleLabel.text = "就從加好友開始吧：）"
        noFriendsTitleLabel.textColor = .textBlack
        
        noFriendsMessageLabel.numberOfLines = 0
        noFriendsMessageLabel.text = "與好友們一起用 KOKO 聊起來！\n還能互相收付款、發紅包喔：）"
        noFriendsMessageLabel.textAlignment = .center
        noFriendsMessageLabel.textColor = .textGray
        
        invitationsTableView.contentInset = .init(top: 10, left: 0, bottom: 0, right: 0 )
        invitationsTableView.register(
            UINib(nibName: InvitationsCell.name, bundle: nil),
            forCellReuseIdentifier: InvitationsCell.name
        )
        invitationsTableView.separatorStyle = .none
        invitationsTableView.rx.setDelegate(self).disposed(by: bag)
        
        searchBgView.backgroundColor = .backgroundGray
        searchBgView.layer.cornerRadius = 10
        
        searchTextField.attributedPlaceholder = "想轉一筆給誰呢？".placeholder
        searchTextField.tintColor = .accentPink
        
        friendListTableView.register(
            UINib(nibName: FriendListCell.name, bundle: nil),
            forCellReuseIdentifier: FriendListCell.name
        )
        friendListTableView.separatorStyle = .none
        friendListTableView.rx.setDelegate(self).disposed(by: bag)
    }
    
    func bindRx() {
        guard let viewModel = viewModel else { return }
        
        viewModel.output.isNoFriends
            .drive(onNext: { [weak self] isNoFriends in
                self?.noFriendsView.isHidden = !isNoFriends
                self?.friendListStackView.isHidden = isNoFriends
            })
            .disposed(by: bag)

        viewModel.output.man
            .drive(onNext: { [weak self] man in
                guard let self = self else { return }
                self.nameLabel.text = man?.name
                self.idLabel.text = "KOKO ID：" + (man?.kokoid ?? "")
            })
            .disposed(by: bag)
        
        viewModel.output.invitations
            .drive(onNext: { [weak self] friends in
                self?.invitationsView.isHidden = friends.isEmpty
            })
            .disposed(by: bag)
        
        viewModel.output.invitations
            .asObservable()
            .bind(to: invitationsTableView.rx.items(cellIdentifier: InvitationsCell.name, cellType: InvitationsCell.self)) { (row, friend, cell) in
                cell.configure(with: friend)
            }
            .disposed(by: bag)
        
        viewModel.output.friendList
            .asObservable()
            .bind(to: friendListTableView.rx.items(cellIdentifier: FriendListCell.name, cellType: FriendListCell.self)) { (row, friend, cell) in
                cell.configure(with: friend)
            }
            .disposed(by: bag)
        
        searchTextField.rx.text
            .orEmpty
            .subscribe(onNext: { [weak self] text in
                guard let self = self,
                      let viewModel = self.viewModel else { return }
                viewModel.input.searchText.onNext(text)
            })
            .disposed(by: bag)
    }
}

extension FriendListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case invitationsTableView:
            return 80
        case friendListTableView:
            return 60
        default:
            return 0
        }
    }
}
