//
//  HomeViewController.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var noFriendsButton: UIButton!
    @IBOutlet private weak var friendListButton: UIButton!
    @IBOutlet private weak var friendListWithInvitaionsButton: UIButton!
    
    private let viewModel = HomeViewModel()
    
    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindRx()
    }
}

private extension HomeViewController {
    
    func bindRx() {
        noFriendsButton.rx
            .tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.input.status.onNext(.noFriends)
            })
            .disposed(by: bag)
        
        friendListButton.rx
            .tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.input.status.onNext(.friendList)
            })
            .disposed(by: bag)
        
        friendListWithInvitaionsButton.rx
            .tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.input.status.onNext(.friendListWithInvitations)
            })
            .disposed(by: bag)
        
        viewModel.output.info
            .drive(
                onNext: { [weak self] info in
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(
                        FriendListViewController.init(with: .init(info: info)),
                        animated: true
                    )
                })
            .disposed(by: bag)
        
        viewModel.output.isLoading
            .drive(onNext: { [weak self] isLoading in
                isLoading ? self?.startLoading() : self?.stopLoading()
            })
            .disposed(by: bag)
    }
}
