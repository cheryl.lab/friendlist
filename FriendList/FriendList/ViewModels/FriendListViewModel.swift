//
//  FriendListViewModel.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/1.
//

import Foundation
import RxSwift
import RxCocoa

class FriendListViewModel: ViewModelType {
    
    typealias Info = HomeViewModel.Info
    
    struct Input {
        let searchText: PublishSubject<String>
    }
    
    struct Output {
        let man: Driver<Man?>
        let invitations: Driver<[Friend]>
        let friendList: Driver<[Friend]>
        let isNoFriends: Driver<Bool>
    }
    
    let input: Input
    
    let output: Output
    
    private let info: Info?
    
    private let searchTextSubject = PublishSubject<String>()
    
    private let manSubject = PublishSubject<Man?>()
    
    private let invitationsSubject = PublishSubject<[Friend]>()
    
    private let friendListSubject = PublishSubject<[Friend]>()
    
    private let isNoFriendsSubject = PublishSubject<Bool>()
    
    private let bag = DisposeBag()
    
    init(info: Info?) {
        self.input = Input(searchText: searchTextSubject)
        self.output = Output(
            man: manSubject.asDriver(onErrorJustReturn: nil),
            invitations: invitationsSubject.asDriver(onErrorJustReturn: []),
            friendList: friendListSubject.asDriver(onErrorJustReturn: []),
            isNoFriends: isNoFriendsSubject.asDriver(onErrorJustReturn: true)
        )
        self.info = info
    }
    
    func start() {
        if let info = info {
            manSubject.onNext(info.mans.first)
            invitationsSubject.onNext(info.friends.filter { $0.status == .pending })
            friendListSubject.onNext(info.friends.filter { $0.status != .pending })
            isNoFriendsSubject.onNext(info.friends.isEmpty)
        }
        bindRx()
    }
}

private extension FriendListViewModel {
    
    func bindRx() {
        searchTextSubject
            .subscribe(onNext: { [weak self] text in
                guard let self = self,
                      let info = self.info else { return }
                
                guard !text.isEmpty else {
                    let friendList = info.friends.filter { $0.status != .pending }
                    self.friendListSubject.onNext(friendList)
                    return
                }
                
                let friendList = info.friends.filter { $0.status != .pending && $0.name.contains(text) }
                self.friendListSubject.onNext(friendList)
            })
            .disposed(by: bag)
    }
}
