//
//  HomeViewModel.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/1.
//

import Foundation
import RxSwift
import RxCocoa

class HomeViewModel: ViewModelType {
    
    enum Status {
        /// 無好友
        case noFriends
        /// 好友列表無邀請
        case friendList
        /// 好友列表含邀請
        case friendListWithInvitations
    }
    
    struct Input {
        let status: PublishSubject<Status>
    }
    
    struct Output {
        let info: Driver<Info?>
        let isLoading: Driver<Bool>
    }
    
    struct Info {
        let mans: [Man]
        let friends: [Friend]
    }
    
    let input: Input
    
    let output: Output
    
    private let repository: Repository
    
    private let statusSubject = PublishSubject<Status>()
    
    private let infoSubject = PublishSubject<Info?>()
    
    private let isLoadingSubject = PublishSubject<Bool>()
    
    private let bag = DisposeBag()
    
    init(repository: Repository = Repository()) {
        self.input = Input(status: statusSubject)
        self.output = Output(
            info: infoSubject.asDriver(onErrorJustReturn: nil),
            isLoading: isLoadingSubject.asDriver(onErrorJustReturn: false)
        )
        self.repository = repository
        
        bindRx()
    }
}

private extension HomeViewModel {
    
    func bindRx() {
        input.status
            .subscribe(onNext: { [weak self] status in
                guard let self = self else { return }
                
                var observable: Observable<Info?>
                
                switch status {
                case .noFriends:
                    observable = self.noFriendsInfo()
                case .friendList:
                    observable = self.friendListInfo()
                case .friendListWithInvitations:
                    observable = self.friendListWithInvitationsInfo()
                }
                
                self.isLoadingSubject.onNext(true)
                
                observable
                    .subscribe(
                        onNext: { [weak self] info in
                            guard let self = self else { return }
                            self.isLoadingSubject.onNext(false)
                            self.infoSubject.onNext(info)
                        },
                        onError: { [weak self] error in
                            guard let self = self else { return }
                            self.isLoadingSubject.onNext(false)
                            print(error)
                        })
                    .disposed(by: self.bag)
            })
            .disposed(by: bag)
    }
    
    func noFriendsInfo() -> Observable<Info?> {
        return Observable
            .zip(repository.man(), repository.friend4())
            .map {
                guard let mans = $0,
                      let friends = $1 else { return nil }
                return Info(mans: mans, friends: friends)
            }
    }
    
    func friendListInfo() -> Observable<Info?> {
        return Observable
            .zip(repository.man(), repository.friend1(), repository.friend2())
            .map {
                guard let mans = $0,
                      let friend1s = $1,
                      let friend2s = $2 else { return nil }
                
                var friends: [Friend] = []
                
                let friend12s = friend1s + friend2s
                
                friend12s.forEach { friend in
                    let target = friend12s
                        .filter { $0.fid == friend.fid }
                        .sorted {
                            guard let updateDate0 = $0.updateDate,
                                  let updateDate1 = $1.updateDate else { return false }
                            return updateDate0 > updateDate1
                        }
                        .first
                    
                    guard let target = target else { return }
                    
                    let isFidNotExisted = friends.filter { $0.fid == target.fid }.isEmpty
                    
                    if isFidNotExisted { friends.append(target) }
                }
                
                return Info(mans: mans, friends: friends)
            }
    }
    
    func friendListWithInvitationsInfo() -> Observable<Info?> {
        return Observable
            .zip(repository.man(), repository.friend3())
            .map {
                guard let mans = $0,
                      let friends = $1 else { return nil }
                return Info(mans: mans, friends: friends)
            }
    }
}
