//
//  Protocols.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/1.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    var input: Input { get }
    var output: Output { get }
}
