//
//  Man.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation

struct Man: Decodable {
    let name: String
    let kokoid: String
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case kokoid = "kokoid"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        kokoid = try container.decode(String.self, forKey: .kokoid)
    }
}
