//
//  Friend.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation

struct Friend: Decodable {
    
    enum Status {
        /// 邀請送出
        case invited
        /// 已完成
        case accepted
        /// 邀請中
        case pending
        /// 無
        case none
        
        init(with statusCode: Int) {
            switch statusCode {
            case 0:
                self = .invited
            case 1:
                self = .accepted
            case 2:
                self = .pending
            default:
                self = .none
            }
        }
    }
    
    let name: String
    let status: Status
    let isTop: Bool
    let fid: String
    let updateDate: Date?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case status = "status"
        case isTop = "isTop"
        case fid = "fid"
        case updateDate = "updateDate"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        
        let statusCode = try container.decode(Int.self, forKey: .status)
        status = Status.init(with: statusCode)
        
        isTop = try container.decode(String.self, forKey: .isTop).boolValue
        
        fid = try container.decode(String.self, forKey: .fid)
        
        updateDate = try container.decode(String.self, forKey: .updateDate).dateValue
    }
}

enum PunctuationMark {
    case slash
    case none
    
    init(with timeString: String) {
        switch true {
        case timeString.contains("/"):
            self = .slash
        default:
            self = .none
        }
    }
}
