//
//  Color+Extension.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/2.
//

import UIKit

extension UIColor {
    // Accent
    static let accentPink = UIColor(named: "accentPink") ?? rgb(red: 236, green: 0, blue: 140)
    
    // Background
    static let backgroundWhite = UIColor(named: "backgroundWhite") ?? rgb(red: 252, green: 252, blue: 252)
    static let backgroundGray = UIColor(named: "backgroundGray") ?? rgb(red: 142, green: 142, blue: 147).withAlphaComponent(0.12)
    
    // Border
    static let borderLightGray = UIColor(named: "borderLightGray") ?? rgb(red: 201, green: 201, blue: 201)
    
    // Text
    static let textBlack = UIColor(named: "textBlack") ?? rgb(red: 71, green: 71, blue: 71)
    static let textGray = UIColor(named: "textGray") ?? rgb(red: 153, green: 153, blue: 153)
    
    // Dividing Line
    static let dividingLine = UIColor(named: "dividingLine") ?? rgb(red: 228, green: 228, blue: 228)
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return .init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1.0)
    }
}
