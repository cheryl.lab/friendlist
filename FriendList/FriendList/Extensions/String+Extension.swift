//
//  String+Extension.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation
import UIKit

extension String {

    var boolValue: Bool {
        self == "1" ? true : false
    }
    
    var dateValue: Date? {
        let formatter = DateFormatter()
        formatter.timeZone = .init(identifier: "GMT")
        switch PunctuationMark.init(with: self) {
        case .slash:
            formatter.dateFormat = "yyyy/MM/dd"
        case .none:
            formatter.dateFormat = "yyyyMMdd"
        }
        return formatter.date(from: self)
    }
    
    var placeholder: NSAttributedString {
        .init(
            string: self,
            attributes: [
                .font: UIFont.systemFont(ofSize: 14),
                .foregroundColor: UIColor.textGray
            ]
        )
    }
}
