//
//  View+Extension.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/11/2.
//

import UIKit

extension UIView {
    static var name: String { return "\(self)" }
}

extension UIViewController {
    
    static var name: String { return "\(self)" }
    
    static let indicatorTag = 999

    func startLoading() {
        guard view.viewWithTag(UIViewController.indicatorTag) == nil else { return }
        let indicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView(style: .gray)
        }
        indicator.center = view.center
        indicator.tag = UIViewController.indicatorTag
        view.addSubview(indicator)
        indicator.startAnimating()
    }

    func stopLoading() {
        guard let indicator = view.viewWithTag(UIViewController.indicatorTag) as? UIActivityIndicatorView else { return }
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
}
