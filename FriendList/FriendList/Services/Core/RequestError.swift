//
//  RequestError.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation

enum RequestError: Error {
    case badRequest(Error)
    case decodingError
}
