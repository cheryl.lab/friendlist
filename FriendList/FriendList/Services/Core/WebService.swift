//
//  WebService.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation
import Alamofire

protocol WebService {
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var queryItems: [URLQueryItem]? { get }
    var parameters: Parameters? { get }
}

extension WebService {
    var baseUrl: String { "https://dimanyen.github.io" }
    var queryItems: [URLQueryItem]? { nil }
}
