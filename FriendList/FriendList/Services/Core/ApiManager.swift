//
//  ApiManager.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation
import Alamofire
import RxSwift

final class ApiManager {
    
    static let shared = ApiManager()
    
    static var session: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return Session(configuration: configuration)
    }()
    
    private let bag = DisposeBag()
    
    func request(_ service: WebService) -> Observable<Data> {
        guard var urlComponent = URLComponents(string: service.baseUrl) else {
            print("[Req] Router = \(type(of: service)); Path = \(service.path); url component is missing")
            return Observable.just(Data())
        }
        urlComponent.path = service.path
        urlComponent.queryItems = service.queryItems
        
        print("[Req] Router = \(type(of: service)); Path = \(service.path)")
        
        return Observable<Data>.create { [urlComponent] observer -> Disposable in
            let request = ApiManager.session.request(
                urlComponent,
                method: service.method,
                parameters: service.parameters,
                encoding: JSONEncoding.default,
                headers: service.headers
            )
            request.responseData { afResponse in
                if let statusCode = afResponse.response?.statusCode {
                    print("[Res] Router = \(type(of: service)); Url = \(urlComponent); Status Code = \(statusCode)")
                } else {
                    print("[Res] Router = \(type(of: service)); Url = \(urlComponent); response is missing")
                }
                
                switch afResponse.result {
                case .success(let data):
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                          let response = json["response"],
                          let data = try? JSONSerialization.data(withJSONObject: response, options: [.prettyPrinted]) else {
                        return observer.onError(RequestError.decodingError)
                    }
                    observer.onNext(data)
                    observer.onCompleted()
                case .failure(let errro):
                    observer.onError(RequestError.badRequest(errro))
                }
            }
            return Disposables.create { request.cancel() }
        }
        .catch { error -> Observable<Data> in
            return .error(RequestError.badRequest(error))
        }
    }
}
