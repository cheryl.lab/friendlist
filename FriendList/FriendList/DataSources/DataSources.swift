//
//  DataSources.swift
//  FriendList
//
//  Created by Cheryl Chen on 2022/10/31.
//

import Foundation
import RxSwift

protocol DataSource {
    func man() -> Observable<[Man]?>
    func friend1() -> Observable<[Friend]?>
    func friend2() -> Observable<[Friend]?>
    func friend3() -> Observable<[Friend]?>
    func friend4() -> Observable<[Friend]?>
}

final class RemoteDataSource: DataSource {
    
    func man() -> Observable<[Man]?> {
        return ApiManager.shared
            .request(Service.man)
            .map({ try JSONDecoder().decode([Man].self, from: $0) })
    }
    
    func friend1() -> Observable<[Friend]?> {
        return ApiManager.shared
            .request(Service.friend1)
            .map({ try JSONDecoder().decode([Friend].self, from: $0) })
    }
    
    func friend2() -> Observable<[Friend]?> {
        return ApiManager.shared
            .request(Service.friend2)
            .map({ try JSONDecoder().decode([Friend].self, from: $0) })
    }
    
    func friend3() -> Observable<[Friend]?> {
        return ApiManager.shared
            .request(Service.friend3)
            .map({ try JSONDecoder().decode([Friend].self, from: $0) })
    }
    
    func friend4() -> Observable<[Friend]?> {
        return ApiManager.shared
            .request(Service.friend4)
            .map({ try JSONDecoder().decode([Friend].self, from: $0) })
    }
}
